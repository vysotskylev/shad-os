#include <stdio.h>
#include <assert.h>

#include "rop.h"

int main()
{
    void* ropchain[1024];
    size_t len = make_ropchain(ropchain);
    int ret = run_ropchain(ropchain, len);
    int expected = 0xdeadbeef;
    if (ret != expected) {
        fprintf(stderr, "error: 0x%x != 0x%x\n", ret, expected);
        return 1;
    }
    return 0;
}
